package com.example.codedemo
import com.example.codedemo.BaseSchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject

interface ResponseDataSource {
    val scheduler: BaseSchedulerProvider
    val DataResponse: PublishSubject<Outcome<Data.Response>>
    fun getDataResponse(disposable: CompositeDisposable)
}