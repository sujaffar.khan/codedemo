package com.example.codedemo

import androidx.lifecycle.LiveData
import com.example.codedemo.base.vm.BaseViewModel


class DataViewModel : BaseViewModel(){
    private val dataRepo = DataRepository.getInstance()
    val allDataOutcome: LiveData<Outcome<Data.Response>> by lazy {
        dataRepo.DataResponse.toLiveData(getCompositeDisposal())
    }

    fun getAllData(){
        dataRepo.getDataResponse(getCompositeDisposal())
    }

}